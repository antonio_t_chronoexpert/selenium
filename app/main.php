<?php declare(strict_types=1);

namespace app;

include '../vendor/autoload.php';

class main
{
    private $pdo;
    private $host = 'stark.chronoexpert.com';
    private $database = 'chrono_qa';
    private $user = 'chrono';
    private $pass = 'NrTWdyG4dvSzPy7w';

    private $wd;

    private $STATUS_DEACTIVATED = 2;
    private $MANAGEMENT_STATE_DELETED_ORIGIN = 5;

    private $conditionStateChange = false;

    public function __construct()
    {
        $this->wd = new webDriver();

        $dsn = "mysql:host=$this->host;dbname=$this->database";
        $this->pdo = new PDO($dsn, $this->user, $this->pass);
    }

    public function detectEraseLeads(string $url, bool $conditionStateChange = false)
    {
        $this->conditionStateChange = $conditionStateChange;
        try {
            $sql = "SELECT
                        id,origin
                    FROM
                        advertisement_leads
                    WHERE
                        status != $this->STATUS_DEACTIVATED
                        AND
                        origin LIKE '" . $url . "%';";
            $stmt = $this->pdo->query($sql);
            while ($row = $stmt->fetch()) {
                $delete = $this->wd->execute($row['origin']);
                if ($delete) {
                    echo "--> " . $row['id'] . " " . $row['origin'] . "\n";
                    $this->deleteLead($row['id']);
                    $id = $this->getIdAdvWithLeadOrigin($row['origin']);
                    if (null != $id) {
                        $this->deleteAdv($id);
                    }
                }
            }

        } catch (PDOException $e) {
            throw $e;
        }
    }

    private function deleteLead(int $id)
    {
        $sql = "UPDATE
                    advertisement_leads
                SET 
                    status = " . $this->STATUS_DEACTIVATED . "
                WHERE
                    id = " . $id;
        $this->pdo->query($sql);
        /*
        $this->logService->addToLog($lead->origin);
        */
    }

    private function getIdAdvWithLeadOrigin(string $leadOrigin): ?int
    {
        $sql = "SELECT 
                    id
                FROM
                    advertisements
                WHERE
                    origin_url = " . $leadOrigin;
        $stmt = $this->pdo->query($sql);
        $row = $stmt->fetch();
        if(null != $row){
            return $row['id'];
        }
        return null;
    }

    private function deleteAdv(int $id)
    {
        $sql = "UPDATE
                    advertisements
                SET";

        if ($this->conditionStateChange) {
            $sql .= "management_state = " . $this->MANAGEMENT_STATE_DELETED_ORIGIN . " AND ";
        }
        $sql .= "       ppc = 0 
                    WHERE
                        id = " . $id;
        $this->pdo->query($sql);

//$this->logService->addToLog($advertisement->origin_url);
    }
}
