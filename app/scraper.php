<?php declare(strict_types=1);

namespace app;

include '../vendor/autoload.php';

//-------------------------------------------------------------------------------

$m = new main();
$m->detectEraseLeads('https://www.iconewatches.com', false);