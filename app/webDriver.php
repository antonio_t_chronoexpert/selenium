<?php declare(strict_types=1);

namespace app;

include '../vendor/autoload.php';

use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Facebook\WebDriver\WebDriverExpectedCondition;


class webDriver
{
    private $driver;

    public function __construct()
    {
        $serverUrl = 'http://localhost:4444/wd/hub';
        $this->driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());
    }

    public function __destruct()
    {
        $this->driver->quit();
    }

    public function execute(string $url): bool
    {
        $this->driver->get($url);
        $this->driver->manage()->window()->setSize(new WebDriverDimension(800, 600));

        $element = $this->driver->wait()->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector('p.price span'))
        );

        return 'No disponible' == $element->getText();
    }
}