<?php
include './vendor/autoload.php';
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Facebook\WebDriver\WebDriverExpectedCondition;

//$driver = ChromeDriver::start();
//$driver->get('https://google.com');

try {


    $serverUrl = 'http://localhost:4444/wd/hub';
    $driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());

    // Go to URL
    $url = 'https://www.iconewatches.com/producto/reloj-breguet-marine-royale-alarm-white-gold/';
    $driver->get($url);
    $driver->manage()->window()->setSize(new WebDriverDimension(800, 600));

    // print title of the current page to output
    echo "The title is '" . $driver->getTitle() . "'\n";

    // print URL of current page to output
    echo "The current URL is '" . $driver->getCurrentURL() . "'\n";


    $element = $driver->wait()->until(
        WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector('p.price span'))
    );

    echo '--> ' . $element->getText();


    $driver->quit();
}
catch (Exception $ex){
    echo $ex->getMessage() . "\n";
}