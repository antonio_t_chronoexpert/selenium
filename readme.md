# Selenium Sobre Local
Test sobre navegador.
Para ejecutar estas pruebas es necesario ejecutarlo sobre un sistema que soporte un navegador.

La librería necesaria para trabajar con el test está en:

**https://github.com/facebook/php-webdriver**

Hay que tener en cuenta que la versión debe coincidir con la versión con la versión de chrome instalada.

Instalar la versión adecuada de chromedriver mediante (en caso necesario):

**sudo ln -s /home/antonio/Projects/cms/tests/Selenium/driver/chromedriver/linux64/chromedriver76 /usr/bin/chromedriver**

**********************************
**Suficiente con esta parte**
**********************************
Ejecutar el servidor selenium como:

    java -Dwebdriver.chrome.driver=chromedriver -jar selenium-server-standalone-3.9.1.jar

Ejecutar el Chromedriver:
    
    /usr/bin/chromedriver

Ejecutar la prueba:
    
    php test.php
***********************************

##NOTAS

Cuidado con las versiones del chromerdriver y el selenium-server-standalone deben están compaginadas con las versión de chrome que esté instalada.

## Selenium Dockerizado

Ejecutar el docker
docker run --net=host selenium/standalone-chrome

Ejecutar la prueba:
En la máquina local!!
cd home/antonio/Projects/cms/tests/Selenium/test 
php test.php


## Docs

https://github.com/php-webdriver/php-webdriver/wiki/Chrome

https://github.com/php-webdriver/php-webdriver


